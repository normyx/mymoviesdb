package com.mgoulene.mmdb.repository;

import java.util.Optional;

import com.mgoulene.domain.Movie;
import com.mgoulene.repository.MovieRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Movie entity.
 */
@Repository
public interface MMDBMovieRepository extends MovieRepository {

    @Query("select movie from Movie movie left join fetch movie.genres where movie.tmdbId =:tmdbId")
    Optional<Movie> findOneWhereTMDBIdWithEagerRelationships(@Param("tmdbId") String id);
}
