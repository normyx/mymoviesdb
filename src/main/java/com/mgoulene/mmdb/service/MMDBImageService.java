package com.mgoulene.mmdb.service;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;

import com.mgoulene.domain.Image;
import com.mgoulene.mmdb.repository.MMDBImageRepository;
import com.mgoulene.mmdb.service.mapper.MMDBImageDTOCopierMapper;
import com.mgoulene.repository.search.ImageSearchRepository;
import com.mgoulene.service.ImageService;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.mapper.ImageMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBImageService extends ImageService {

    private final Logger log = LoggerFactory.getLogger(MMDBImageService.class);

    private final MMDBImageRepository mmdbImageRepository;

    public MMDBImageService(ImageMapper imageMapper,
            ImageSearchRepository imageSearchRepository, MMDBImageRepository mmdbImageRepository) {
        super(mmdbImageRepository, imageMapper, imageSearchRepository);
        this.mmdbImageRepository = mmdbImageRepository;
    }

    
    /*private byte[] scale(byte[] sourceData, int width, int height) {
        ByteArrayInputStream in = new ByteArrayInputStream(sourceData);
        try {
            BufferedImage img = ImageIO.read(in);
            if(height == 0) {
                height = (width * img.getHeight())/ img.getWidth(); 
            }
            if(width == 0) {
                width = (height * img.getWidth())/ img.getHeight();
            }
            java.awt.Image scaledImage = img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0,0,0), null);
    
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    
            ImageIO.write(imageBuff, "jpg", buffer);
    
            return buffer.toByteArray();
        } catch (IOException e) {
            log.error("IOException in scale");
            return null;
        }
    }

    private byte[] scale(byte[] sourceData, ImageSize size) {
        switch (size) {
            case W1280 : return scale(sourceData, 1260, 0);
            case H632 : return scale(sourceData, 0, 632);
            case W185 : return scale(sourceData, 185, 0);
            case W300 : return scale(sourceData, 300, 0);
            case W500 : return scale(sourceData, 500, 0);
            case W780 : return scale(sourceData, 780, 0);
            case ORIGINAL : return sourceData;
            default : 
                log.error("Image Size not taken into account: {}", size);
                return null;
        }
    }*/

    
    
    public Optional<ImageDTO> findOneWhereTMDBId(String tmdbId) {
        log.debug("Request to get Picture: {}", tmdbId);
        Optional<ImageDTO> imageDTOOpt =  mmdbImageRepository.findOneWhereTMDBId(tmdbId).map(imageMapper::toDto);
        if (imageDTOOpt.isPresent()) {
            return imageDTOOpt;
        }
        return Optional.empty();
        
    }

    @Override
    public ImageDTO save(ImageDTO imageDTO) {
        imageDTO.setLastTMDBUpdate(LocalDate.now());
        return super.save(imageDTO);
    }


    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedMoviePoster(Long movieId) {
        log.debug("Request to get Poster from movie: {}", movieId);
        List<Image> posters = mmdbImageRepository.findMoviePosters(movieId);
        if (posters != null && posters.size() != 0) {
            return Optional.of(imageMapper.toDto(posters.get(0)));
        } else {
            return Optional.empty();
        }

    }


    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedMovieBackdrop(Long movieId) {
        log.debug("Request to get Backdrop from movie: {}", movieId);
        List<Image> backdrops = mmdbImageRepository.findMovieBacksrops(movieId);
        if (backdrops != null && backdrops.size() != 0) {
            return Optional.of(imageMapper.toDto(backdrops.get(0)));
        } else {
            return Optional.empty();
        }

    }

    @Transactional(readOnly = true)
    public Optional<ImageDTO> findPreferedPersonImage(Long personId) {
        log.debug("Request to get Image from person: {}", personId);
        List<Image> images = mmdbImageRepository.findPersonImages(personId);
        if (images != null && images.size() != 0) {
            return Optional.of(imageMapper.toDto(images.get(0)));
        } else {
            return Optional.empty();
        }

    }


    

}
