package com.mgoulene.mmdb.service;

import com.mgoulene.repository.CreditRepository;
import com.mgoulene.repository.GenreRepository;
import com.mgoulene.repository.MovieRepository;
import com.mgoulene.repository.MyMovieRepository;
import com.mgoulene.repository.PersonRepository;
import com.mgoulene.repository.search.CreditSearchRepository;
import com.mgoulene.repository.search.GenreSearchRepository;
import com.mgoulene.repository.search.MovieSearchRepository;
import com.mgoulene.repository.search.MyMovieSearchRepository;
import com.mgoulene.repository.search.PersonSearchRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MMDBService {

    private final PersonSearchRepository personSearchRepository;
    private final PersonRepository personRepository;

    private final MovieSearchRepository movieSearchRepository;
    private final MovieRepository movieRepository;

    private final CreditSearchRepository creditSearchRepository;
    private final CreditRepository creditRepository;

    private final GenreSearchRepository genreSearchRepository;
    private final GenreRepository genreRepository;

    private final MyMovieSearchRepository myMovieSearchRepository;
    private final MyMovieRepository myMovieRepository;

    private final Logger log = LoggerFactory.getLogger(MMDBService.class);

    public MMDBService(PersonSearchRepository personSearchRepository, PersonRepository personRepository,
            MovieSearchRepository movieSearchRepository, MovieRepository movieRepository,
            CreditSearchRepository creditSearchRepository, CreditRepository creditRepository,
            GenreSearchRepository genreSearchRepository, GenreRepository genreRepository,
            MyMovieSearchRepository myMovieSearchRepository, MyMovieRepository myMovieRepository) {
        this.personSearchRepository = personSearchRepository;
        this.personRepository = personRepository;
        this.movieRepository = movieRepository;
        this.movieSearchRepository = movieSearchRepository;
        this.creditRepository = creditRepository;
        this.creditSearchRepository = creditSearchRepository;
        this.genreRepository = genreRepository;
        this.genreSearchRepository = genreSearchRepository;
        this.myMovieRepository = myMovieRepository;
        this.myMovieSearchRepository = myMovieSearchRepository;

    }

    public void resetElasticSearch() {
        log.debug("Reset ElasticSearch : Delete Person");
        personSearchRepository.deleteAll();
        log.debug("Reset ElasticSearch : Create Person");
        personSearchRepository.saveAll(personRepository.findAll());
        log.debug("Reset ElasticSearch : Delete Movie");
        movieSearchRepository.deleteAll();
        log.debug("Reset ElasticSearch : Create Movie");
        movieSearchRepository.saveAll(movieRepository.findAll());
        log.debug("Reset ElasticSearch : Delete Credit");
        creditSearchRepository.deleteAll();
        log.debug("Reset ElasticSearch : Create Credit");
        creditSearchRepository.saveAll(creditRepository.findAll());
        log.debug("Reset ElasticSearch : Delete Genre");
        genreSearchRepository.deleteAll();
        log.debug("Reset ElasticSearch : Create Genre");
        genreSearchRepository.saveAll(genreRepository.findAll());
        log.debug("Reset ElasticSearch : Delete MyMovie");
        myMovieSearchRepository.deleteAll();
        log.debug("Reset ElasticSearch : Create MyMovie");
        myMovieSearchRepository.saveAll(myMovieRepository.findAll());

    }

}
