package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.domain.Movie;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.dto.MovieDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MMDBImageDTOCopierMapper {

    @Mapping(target = "id", ignore = true)
    ImageDTO copy(ImageDTO source);


}
