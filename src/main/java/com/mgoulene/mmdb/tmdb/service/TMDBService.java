package com.mgoulene.mmdb.tmdb.service;

import java.util.Optional;


import com.mgoulene.mmdb.service.MMDBCreditService;
import com.mgoulene.mmdb.service.MMDBGenreService;
import com.mgoulene.mmdb.service.MMDBImageService;
import com.mgoulene.mmdb.service.MMDBMovieService;
import com.mgoulene.mmdb.service.MMDBPersonService;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBCreditDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBGenreDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBImageDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieCreditResponse;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieImageDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBPersonDTO;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBPersonImageDTO;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaCreditEvent;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaImageEvent;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaMovieEvent;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaCreditEvent.KafkaCreditAction;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaImageEvent.KafkaImageAction;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaMovieEvent.KafkaMovieAction;
import com.mgoulene.mmdb.tmdb.service.kafka.producer.TMDBCreditProducer;
import com.mgoulene.mmdb.tmdb.service.kafka.producer.TMDBImageProducer;
import com.mgoulene.mmdb.tmdb.service.kafka.producer.TMDBMovieProducer;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBCreditMapper;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBGenreMapper;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBMovieMapper;
import com.mgoulene.mmdb.tmdb.service.mapper.TMDBPersonMapper;
import com.mgoulene.service.dto.CreditDTO;
import com.mgoulene.service.dto.GenreDTO;
import com.mgoulene.service.dto.ImageDTO;
import com.mgoulene.service.dto.MovieDTO;
import com.mgoulene.service.dto.PersonDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TMDBService {

    private final Logger log = LoggerFactory.getLogger(TMDBService.class);

    private final TMDBMovieMapper tmdbMovieMapper;

    private final TMDBCreditMapper tmdbCreditMapper;

    private final TMDBPersonMapper tmdbPersonMapper;

    private final TMDBGenreMapper tmdbGenreMapper;

    private final MMDBGenreService mmdbGenreService;

    private final MMDBMovieService movieService;

    private final MMDBCreditService creditService;

    private final MMDBPersonService personService;

    private final TMDBMovieProducer movieProducer;

    private final TMDBCreditProducer creditProducer;

    private final TMDBImageProducer imageProducer;

    private final MMDBImageService imageService;

    private final TMDBAPIService tmdbApiService;

    public TMDBService(TMDBMovieMapper tmdbMovieMapper, MMDBMovieService movieService, TMDBGenreMapper tmdbGenreMapper,
            TMDBCreditMapper tmdbCreditMapper, TMDBMovieProducer movieProducer, MMDBCreditService creditService,
            TMDBCreditProducer creditProducer, MMDBPersonService personService, TMDBPersonMapper tmdbPersonMapper,
            TMDBAPIService tmdbApiService, TMDBImageProducer imageProducer, MMDBImageService imageService, MMDBGenreService mmdbGenreService) {

        this.tmdbMovieMapper = tmdbMovieMapper;
        this.movieService = movieService;

        this.creditService = creditService;
        this.tmdbCreditMapper = tmdbCreditMapper;
        this.movieProducer = movieProducer;

        this.creditProducer = creditProducer;
        this.personService = personService;
        this.tmdbPersonMapper = tmdbPersonMapper;

        this.tmdbApiService = tmdbApiService;
        this.imageProducer = imageProducer;
        this.imageService = imageService;

        this.mmdbGenreService = mmdbGenreService;
        this.tmdbGenreMapper = tmdbGenreMapper;
    }

    
    public void createOrUpdateCreditsFromTMDBMovie(String tmdbMovieId) {
        log.debug("Request to import credits from Movie  {}", tmdbMovieId);
        Optional<MovieDTO> movieDTOOpt = movieService.findOneWhereTMDBId(tmdbMovieId);
        if (movieDTOOpt.isPresent()) {
            MovieDTO movieDTO = movieDTOOpt.get();
            TMDBMovieCreditResponse creditsResponse = tmdbApiService.findTMDBCreditsFromMovie(tmdbMovieId);
            for (TMDBCreditDTO cast : creditsResponse.getCast()) {
                cast.setCreditType("Cast");
                cast.setMovieId(movieDTO.getId());
                KafkaCreditEvent event = new KafkaCreditEvent(KafkaCreditAction.INSERT_OR_UPDATE);
                event.setTmdbCreditDTO(cast);
                creditProducer.send(event);
            }
            for (TMDBCreditDTO crew : creditsResponse.getCrew()) {
                crew.setCreditType("Crew");
                crew.setMovieId(movieDTO.getId());
                KafkaCreditEvent event = new KafkaCreditEvent(KafkaCreditAction.INSERT_OR_UPDATE);
                event.setTmdbCreditDTO(crew);
                creditProducer.send(event);
            }
        } else {
            log.error("Movie not found  {}", tmdbMovieId);
        }
    }

    public MovieDTO createOrUpdateMovieFromTMDB(String tmdbId) {
        Optional<MovieDTO> movieDTOOpt = movieService.findOneWhereTMDBId(tmdbId);
        MovieDTO movieDTO;

        TMDBMovieDTO tmdbMovieDTO = tmdbApiService.findOneTMDBMovie(tmdbId);
        log.debug("Movie found : {}", tmdbMovieDTO);
        String posterPath = tmdbMovieDTO.getPosterPath();
        // If movie already exist, then do not import
        if (movieDTOOpt.isPresent()) {
            log.debug("Do not import already existing Movie, update it : {} to {}", movieDTOOpt.get(), tmdbMovieDTO);
            movieDTO = movieDTOOpt.get();
            tmdbMovieMapper.updateDTO(tmdbMovieDTO, movieDTO);
            //movieDTO = movieService.saveAndCommit(movieDTO);

        } else {
            log.debug("Import Movie: {}", tmdbMovieDTO);
            movieDTO = tmdbMovieMapper.toDTO(tmdbMovieDTO);
        }
        
        // Find Genres
        for (TMDBGenreDTO tmdbGenreDTO:tmdbMovieDTO.getGenres()) {
            // Find if genre already exists
            Optional<GenreDTO> genreDTOOpt = mmdbGenreService.findOneWhereTMDBId(tmdbGenreDTO.getId());
            if (genreDTOOpt.isPresent()) {
                movieDTO.getGenres().add(genreDTOOpt.get());
            } else {
                GenreDTO genreDTO = tmdbGenreMapper.toDTO(tmdbGenreDTO);
                genreDTO = mmdbGenreService.saveAndCommit(genreDTO);
                movieDTO.getGenres().add(genreDTO);
            }
        }
        movieDTO = movieService.saveAndCommit(movieDTO);
        KafkaMovieEvent movieEvent = new KafkaMovieEvent(KafkaMovieAction.INSERT_DEPENDENCIES, movieDTO);
        movieProducer.send(movieEvent);

        return movieDTO;
    }

    public void createOrUpdateMovieDependenciesFromTMDB(MovieDTO movieDTO) {
        KafkaCreditEvent creditEvent = new KafkaCreditEvent(KafkaCreditAction.INSERT_OR_UPDATE_ALL_FROM_MOVIE);
        creditEvent.setTmdbMovieId(movieDTO.getTmdbId());
        creditProducer.send(creditEvent);
        KafkaImageEvent imageEvent = new KafkaImageEvent(KafkaImageAction.INSERT_OR_UPDATE_MOVIE_IMAGES);
        imageEvent.setRelatedTmdbId(movieDTO.getTmdbId());
        imageProducer.send(imageEvent);
    }

    public void createOrUpdateCreditFromTMDB(TMDBCreditDTO tmdbCreditDTO) {
        log.debug("importCreditFromTMDB: {}", tmdbCreditDTO);
        Optional<CreditDTO> creditDTOOpt = creditService.findOneWhereTMDBId(tmdbCreditDTO.getTmdbId());
        CreditDTO creditDTO;
        if (creditDTOOpt.isPresent()) {
            creditDTO = creditDTOOpt.get();
            tmdbCreditMapper.updateDTO(tmdbCreditDTO, creditDTO);
        } else {
            creditDTO = tmdbCreditMapper.toDTO(tmdbCreditDTO);
        }
        Optional<PersonDTO> personDTOOpt = personService.findOneWhereTMDBId(tmdbCreditDTO.getTmdbPersonId());
        PersonDTO personDTO;
        TMDBPersonDTO tmdbPersonDTO = tmdbApiService.findOneTMDBPerson(tmdbCreditDTO.getTmdbPersonId());
        if (personDTOOpt.isPresent()) {
            personDTO = personDTOOpt.get();
            tmdbPersonMapper.updateDTO(tmdbPersonDTO, personDTO);
            log.debug("Person already exist: {}", personDTO);
        } else {
            personDTO = tmdbPersonMapper.toDTO(tmdbPersonDTO);
            log.debug("Creating new Person: {}", personDTO);
        }
        personDTO = personService.save(personDTO);
        KafkaImageEvent imageEvent = new KafkaImageEvent(KafkaImageAction.INSERT_OR_UPDATE_PERSON_ALL_PROFILES);
        imageEvent.setRelatedTmdbId(personDTO.getTmdbId());
        imageProducer.send(imageEvent);

        creditDTO.setPersonId(personDTO.getId());
        log.debug("Creating new Credit: {}", personDTO);
        creditService.save(creditDTO);
    }

    public void createOrUpdateMovieImagesFromTMDB(String tmdbMovieId) {
        log.debug("createOrUpdateMovieImagesFromTMDB for movie {} ", tmdbMovieId);
        TMDBMovieImageDTO movieImageDTO = tmdbApiService.findTMDBMovieImages(tmdbMovieId);
        for (TMDBImageDTO backdrop : movieImageDTO.getBackdrops()) {
            KafkaImageEvent event = new KafkaImageEvent(KafkaImageAction.INSERT_OR_UPDATE_MOVIE_BACKDROP);
            event.setImage(backdrop);
            event.setRelatedTmdbId(tmdbMovieId);
            imageProducer.send(event);
        }
        for (TMDBImageDTO poster : movieImageDTO.getPosters()) {
            KafkaImageEvent event = new KafkaImageEvent(KafkaImageAction.INSERT_OR_UPDATE_MOVIE_POSTER);
            event.setImage(poster);
            event.setRelatedTmdbId(tmdbMovieId);
            imageProducer.send(event);
        }
    }

    public void createOrUpdatePersonImagesFromTMDB(String tmdbPersonId) {
        log.debug("createOrUpdatePersonImagesFromTMDB for person {} ", tmdbPersonId);
        TMDBPersonImageDTO personImageDTO = tmdbApiService.findTMDBPersonImages(tmdbPersonId);
        for (TMDBImageDTO profile : personImageDTO.getProfiles()) {
            KafkaImageEvent event = new KafkaImageEvent(KafkaImageAction.INSERT_OR_UPDATE_PERSON_PROFILE);
            event.setImage(profile);
            event.setRelatedTmdbId(tmdbPersonId);
            imageProducer.send(event);
        }
    }

    public ImageDTO getImageDTO(TMDBImageDTO image) {
        Optional<ImageDTO> imageDTOOpt = imageService.findOneWhereTMDBId(image.getFilePath());
        ImageDTO imageDTO;
        if (imageDTOOpt.isPresent()) {
            imageDTO = imageDTOOpt.get();
        } else {
            imageDTO = new ImageDTO();
        }
        imageDTO.setTmdbId(image.getFilePath());
        //tmdbApiService.findOneTMDBImageBytes(imageDTO);
        //imageDTO.setImageSize(ImageSize.ORIGINAL);
        imageDTO.setLocale(image.getLocale());
        imageDTO.setTmdbId(image.getFilePath());
        imageDTO.setVoteAverage(image.getVoteAverage());
        imageDTO.setVoteCount(image.getVoteCount());
        return imageDTO;
    }

    public void createOrUpdateMovieBackdrop(String movieTmdbId, TMDBImageDTO image) {
        log.debug("createOrUpdateMovieBackdrop for movie {} : {}", movieTmdbId, image);
        Optional<MovieDTO> movieDTOOpt = movieService.findOneWhereTMDBId(movieTmdbId);
        if (movieDTOOpt.isPresent()) {
            MovieDTO movieDTO = movieDTOOpt.get();

            ImageDTO imageDTO = getImageDTO(image);
            imageDTO.setBackdropMovieId(movieDTO.getId());
            imageService.save(imageDTO);
        }
    }

    public void createOrUpdateMoviePoster(String movieTmdbId, TMDBImageDTO image) {
        log.debug("createOrUpdateMovieBackdrop for movie {} : {}", movieTmdbId, image);
        Optional<MovieDTO> movieDTOOpt = movieService.findOneWhereTMDBId(movieTmdbId);
        if (movieDTOOpt.isPresent()) {
            MovieDTO movieDTO = movieDTOOpt.get();
            ImageDTO imageDTO = getImageDTO(image);
            imageDTO.setPosterMovieId(movieDTO.getId());
            imageService.save(imageDTO);
        }
    }

    public void createOrUpdatePersonProfile(String personTmdbId, TMDBImageDTO image) {
        log.debug("createOrUpdatePersonProfile for Person {} : {}", personTmdbId, image);
        Optional<PersonDTO> personDTOOpt = personService.findOneWhereTMDBId(personTmdbId);
        if (personDTOOpt.isPresent()) {
            PersonDTO personDTO = personDTOOpt.get();
            ImageDTO imageDTO = getImageDTO(image);
            imageDTO.setPersonId(personDTO.getId());
            imageService.save(imageDTO);
        }
    }
}
