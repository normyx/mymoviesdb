package com.mgoulene.mmdb.tmdb.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A DTO for the {@link com.mgoulene.domain.TMDBMovie} entity.
 */
public class TMDBMovieDTO implements Serializable {

    private Long id;

    private String title;

    @JsonProperty("adult")
    private Boolean forAdult;

    private String homepage;

    @JsonProperty("original_language")
    private String originalLangage;

    @JsonProperty("original_title")
    private String originalTitle;

    private String overview;

    private String tagline;

    private String status;

    @JsonProperty("vote_average")
    private Float voteAverage;

    @JsonProperty("vote_count")
    private Integer voteCount;

    @JsonProperty("release_date")
    private LocalDate releaseDate;

    @JsonProperty("poster_path")
    private String posterPath;

    private Integer runtime;

    @JsonProperty
    private TMDBGenreDTO[] genres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean isForAdult() {
        return forAdult;
    }

    public void setForAdult(Boolean forAdult) {
        this.forAdult = forAdult;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getOriginalLangage() {
        return originalLangage;
    }

    public void setOriginalLangage(String originalLangage) {
        this.originalLangage = originalLangage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public TMDBGenreDTO[] getGenres() {
        return genres;
    }

    public void setGenres(TMDBGenreDTO[] genres) {
        this.genres = genres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TMDBMovieDTO)) {
            return false;
        }

        return id != null && id.equals(((TMDBMovieDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TMDBMovieDTO [forAdult=" + forAdult + ", genres=" + Arrays.toString(genres) + ", homepage=" + homepage
                + ", id=" + id + ", originalLangage=" + originalLangage + ", originalTitle=" + originalTitle
                + ", overview=" + overview + ", posterPath=" + posterPath + ", releaseDate=" + releaseDate
                + ", runtime=" + runtime + ", status=" + status + ", tagline=" + tagline + ", title=" + title
                + ", voteAverage=" + voteAverage + ", voteCount=" + voteCount + "]";
    }

    
    

    

    


}
