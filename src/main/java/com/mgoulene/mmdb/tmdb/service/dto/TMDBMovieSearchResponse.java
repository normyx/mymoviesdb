package com.mgoulene.mmdb.tmdb.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TMDBMovieSearchResponse {

  @JsonProperty
  private TMDBMovieDTO[] results;
  
  public TMDBMovieDTO[] getResults() {
    return results;
  }

  public void setResults(TMDBMovieDTO[] results) {
    this.results = results;
  }

  public TMDBMovieSearchResponse(){
    
  }

  @Override
  public String toString() {
      String res = "Response [results=\n";
      for (int i = 0; i < results.length; i++ ) {
          res += results[i]+"\n";
      }
      res += "]";
    return  res;
  }

}