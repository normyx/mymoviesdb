package com.mgoulene.mmdb.tmdb.service.kafka.consumer;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.service.kafka.consumer.MMDBGenericConsumer;
import com.mgoulene.mmdb.service.kafka.deserializer.MMDBDeserializationError;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaCreditEvent;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import io.vavr.control.Either;

//@Service
public class TMDBCreditConsumer extends MMDBGenericConsumer<KafkaCreditEvent> {

    private final Logger log = LoggerFactory.getLogger(TMDBCreditConsumer.class);

    private TMDBService tmdbService;

    public TMDBCreditConsumer(@Value("${kafka.consumer.tmdbcredit.name}") final String topicName,
            final KafkaProperties kafkaProperties, TMDBService tmdbService) {
        super(topicName, kafkaProperties.getConsumer().get("tmdbcredit"), kafkaProperties.getPollingTimeout());

        this.tmdbService = tmdbService;

    }

    @Override
    protected void handleMessage(final ConsumerRecord<String, Either<MMDBDeserializationError, KafkaCreditEvent>> record) {
        final Either<MMDBDeserializationError, KafkaCreditEvent> value = record.value();

        if (value.isLeft()) {
            log.error("Deserialization record failure: {}", value.getLeft());
        } else {
            log.debug("topic = {}, partition = {}, offset = {},id = {}", record.topic(),
                    record.partition(), record.offset(), this, record.key(), record.value());
            // log.debug("Handling record: {}", value.get());
        }

        // TODO : Checks if Credit Exists

        KafkaCreditEvent creditEvent = value.get();
        if (creditEvent.getAction() == KafkaCreditEvent.KafkaCreditAction.INSERT_OR_UPDATE_ALL_FROM_MOVIE) {
            String tmdbMovieId = creditEvent.getTmdbMovieId();
            tmdbService.createOrUpdateCreditsFromTMDBMovie(tmdbMovieId);
        } else if (creditEvent.getAction() == KafkaCreditEvent.KafkaCreditAction.INSERT_OR_UPDATE) {
            tmdbService.createOrUpdateCreditFromTMDB(creditEvent.getTmdbCreditDTO());
        } else {
            log.error("Action not recognize: {}", creditEvent);

        }

    }

    @Bean
    public void executeKafkaTMDBCreditRunner() {
        new SimpleAsyncTaskExecutor().execute(this);
    }
}
