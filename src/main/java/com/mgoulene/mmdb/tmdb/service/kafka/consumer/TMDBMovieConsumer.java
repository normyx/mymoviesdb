package com.mgoulene.mmdb.tmdb.service.kafka.consumer;

import com.mgoulene.config.KafkaProperties;
import com.mgoulene.mmdb.service.kafka.consumer.MMDBGenericConsumer;
import com.mgoulene.mmdb.service.kafka.deserializer.MMDBDeserializationError;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaMovieEvent;
import com.mgoulene.service.dto.MovieDTO;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import io.vavr.control.Either;

public class TMDBMovieConsumer extends MMDBGenericConsumer<KafkaMovieEvent> {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieConsumer.class);

    private TMDBService tmdbService;

    public TMDBMovieConsumer(final String topicName,
            final KafkaProperties kafkaProperties, TMDBService tmdbService) {
        super(topicName, kafkaProperties.getConsumer().get("tmdbmovie"), kafkaProperties.getPollingTimeout());
        this.tmdbService = tmdbService;
    }

    @Override
    protected void handleMessage(final ConsumerRecord<String, Either<MMDBDeserializationError, KafkaMovieEvent>> record) {
        final Either<MMDBDeserializationError, KafkaMovieEvent> value = record.value();

        if (value.isLeft()) {
            log.error("Deserialization record failure: {}", value.getLeft());
        } else {
            // Maybe you could delete the next log.info(...) to avoid disclosing personal
            // user information
            log.debug("Handling record: {}", value.get());
        }

        KafkaMovieEvent kafkaMovieDTO = value.get();
        System.out.println("Value :" + kafkaMovieDTO);
        if (kafkaMovieDTO.getAction().equals(KafkaMovieEvent.KafkaMovieAction.INSERT_DEPENDENCIES)) {
            MovieDTO movieDTO = value.get().getMovie();
            log.debug("Inserting movie dependencies : {}", movieDTO);
            tmdbService.createOrUpdateMovieDependenciesFromTMDB(movieDTO);

        }
    }

    @Bean
    public void executeKafkTMDBMovieRunner() {
        new SimpleAsyncTaskExecutor().execute(this);
    }
}
