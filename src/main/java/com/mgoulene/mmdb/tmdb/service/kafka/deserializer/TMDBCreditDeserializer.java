package com.mgoulene.mmdb.tmdb.service.kafka.deserializer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.mgoulene.mmdb.service.kafka.deserializer.MMDBDeserializationError;
import com.mgoulene.mmdb.tmdb.service.kafka.event.KafkaCreditEvent;

import io.vavr.control.Either;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;


public class TMDBCreditDeserializer implements Deserializer<Either<MMDBDeserializationError, KafkaCreditEvent>> {

    private final Logger log = LoggerFactory.getLogger(TMDBCreditDeserializer.class);

    private final ObjectMapper objectMapper;

    private String encoding = "UTF8";

    public TMDBCreditDeserializer() {
        this.objectMapper =
            new ObjectMapper()
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule())
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                .setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE)
                .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
                .setDateFormat(new StdDateFormat());
    }

    @Override
    public Either<MMDBDeserializationError, KafkaCreditEvent> deserialize(final String topicName, final byte[] data) {
        try {
            final KafkaCreditEvent value = objectMapper.readValue(data, KafkaCreditEvent.class);
            return Either.right(value);
        } catch (final IOException e) {
            return Either.left(new MMDBDeserializationError(data, e));
        }
    }
}
