package com.mgoulene.mmdb.tmdb.service.kafka.event;

import java.io.Serializable;

import com.mgoulene.service.dto.MovieDTO;

public class KafkaMovieEvent  implements Serializable{

    public enum KafkaMovieAction {
        INSERT_DEPENDENCIES,
        INSERT_OR_UPDATE
    }

    private static final long serialVersionUID = 1L;
    //public static final String INSERT="Insert";

    public KafkaMovieAction action;

    public MovieDTO movie;

    public KafkaMovieEvent() {
    }

    public KafkaMovieEvent(KafkaMovieAction action, MovieDTO movie) {
        this.action = action;
        this.movie = movie;
    }
    
    public KafkaMovieAction getAction() {
        return action;
    }

    public void setAction(KafkaMovieAction action) {
        this.action = action;
    }

    public MovieDTO getMovie() {
        return movie;
    }

    public void setMovie(MovieDTO movie) {
        this.movie = movie;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((movie == null) ? 0 : movie.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KafkaMovieEvent other = (KafkaMovieEvent) obj;
        if (action != other.action)
            return false;
        if (movie == null) {
            if (other.movie != null)
                return false;
        } else if (!movie.equals(other.movie))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "KafkaMovieEvent [action=" + action + ", movie=" + movie + "]";
    }

    

   
    
    
    
}
