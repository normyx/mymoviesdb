package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.domain.Movie;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBCreditDTO;
import com.mgoulene.service.dto.CreditDTO;
import com.mgoulene.service.dto.MovieDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBCreditMapper extends TMDBMapper<TMDBCreditDTO, CreditDTO> {

    
    CreditDTO toDTO(TMDBCreditDTO tmdbCreditDTO);

    @Mapping(target = "id", ignore = true)
    void updateDTO(TMDBCreditDTO tmdbCreditDTO, @MappingTarget CreditDTO creditDTO);

}
