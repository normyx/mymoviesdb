package com.mgoulene.mmdb.tmdb.service.mapper;

import com.mgoulene.domain.*;
import com.mgoulene.service.dto.MovieDTO;
import com.mgoulene.service.mapper.EntityMapper;
import com.mgoulene.mmdb.tmdb.service.dto.TMDBMovieDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Movie} and its DTO {@link MovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TMDBMovieMapper extends TMDBMapper<TMDBMovieDTO, MovieDTO> {

    @Mapping(target = "genres", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    //@Mapping(target = "posterTmdbId", source = "posterPath")
    MovieDTO toDTO(TMDBMovieDTO tmdbMovieDTO);

    @Mapping(target = "genres", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "tmdbId", source = "id")
    //@Mapping(target = "posterTmdbId", source = "posterPath")
    void updateDTO(TMDBMovieDTO tmdbMovieDTO,  @MappingTarget MovieDTO movieDTO);

}
