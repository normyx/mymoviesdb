package com.mgoulene.mmdb.tmdb.web.rest;

import java.util.List;
import java.util.Optional;

import com.mgoulene.mmdb.tmdb.service.TMDBAPIService;
import com.mgoulene.mmdb.tmdb.service.TMDBService;
import com.mgoulene.service.dto.MovieDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mgoulene.domain.TMDBMovie}.
 */
@RestController
@RequestMapping("/api")
public class TMDBMovieResource {

    private final Logger log = LoggerFactory.getLogger(TMDBMovieResource.class);

    private final TMDBService tmdbService;
    private final TMDBAPIService tmdbApiService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public TMDBMovieResource(TMDBService tmdbService, TMDBAPIService tmdbApiService) {
        this.tmdbService = tmdbService;
        this.tmdbApiService = tmdbApiService;
    }

    /**
     * {@code SEARCH  /_search/tmdb-movies?query=:query} : search for the tMDBMovie
     * corresponding to the query.
     *
     * @param query    the query of the tMDBMovie search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/tmdb-movies")
    public List<MovieDTO> searchTMDBMovies(@RequestParam String query) {
        log.debug("REST request to search for TMDBMovies::searchTMDBMovies for query {}", query);
        return tmdbApiService.searchTMDBMovies(query);
    }

    /**
     * {@code GET  /movies/:id} : get the "id" movie.
     *
     * @param id the id of the movieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the movieDTO, or with status {@code 404 (Not Found)}.
     */
    /*@GetMapping("/tmdb-movies/{tmdbId}")
    public ResponseEntity<MovieDTO> getMovie(@PathVariable String tmdbId) {
        log.debug("REST request to get TMDBMovieDTO : {}", tmdbId);
        MovieDTO movieDTO = tmdbService.findOneTMDBMovie(tmdbId);
        return ResponseUtil.wrapOrNotFound(Optional.of(movieDTO));
    }*/

    @GetMapping("/import-tmdb-movie/{tmdbId}")
    public ResponseEntity<MovieDTO> importMovie(@PathVariable String tmdbId) {
        log.debug("REST request to import TMDBMovieDTO : {}", tmdbId);
        MovieDTO movieDTO = tmdbService.createOrUpdateMovieFromTMDB(tmdbId);
        return ResponseUtil.wrapOrNotFound(Optional.of(movieDTO));
    }
}
