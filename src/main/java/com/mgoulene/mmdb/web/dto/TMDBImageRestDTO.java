package com.mgoulene.mmdb.web.dto;

public class TMDBImageRestDTO {
    private String tmdbId;
    

    public TMDBImageRestDTO() {
    }
    public TMDBImageRestDTO(String tmdbId) {
        this.tmdbId = tmdbId;
        
    }
    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    
    

    
    
}
