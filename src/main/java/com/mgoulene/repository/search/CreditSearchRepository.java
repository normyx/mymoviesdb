package com.mgoulene.repository.search;

import com.mgoulene.domain.Credit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Credit} entity.
 */
public interface CreditSearchRepository extends ElasticsearchRepository<Credit, Long> {
}
