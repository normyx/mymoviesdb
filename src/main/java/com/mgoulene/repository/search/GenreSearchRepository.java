package com.mgoulene.repository.search;

import com.mgoulene.domain.Genre;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Genre} entity.
 */
public interface GenreSearchRepository extends ElasticsearchRepository<Genre, Long> {
}
