package com.mgoulene.repository.search;

import com.mgoulene.domain.ImageData;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ImageData} entity.
 */
public interface ImageDataSearchRepository extends ElasticsearchRepository<ImageData, Long> {
}
