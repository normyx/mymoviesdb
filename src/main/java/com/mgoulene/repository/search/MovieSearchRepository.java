package com.mgoulene.repository.search;

import com.mgoulene.domain.Movie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Movie} entity.
 */
public interface MovieSearchRepository extends ElasticsearchRepository<Movie, Long> {
}
