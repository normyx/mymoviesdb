package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.domain.Genre} entity.
 */
@ApiModel(description = "The Genre entity.\n@author A true hipster")
public class GenreDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String tmdbId;

    @NotNull
    private String name;

    @NotNull
    private LocalDate lastTMDBUpdate;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GenreDTO)) {
            return false;
        }

        return id != null && id.equals(((GenreDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "GenreDTO{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", name='" + getName() + "'" +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            "}";
    }
}
