package com.mgoulene.service.dto;

import io.swagger.annotations.ApiModel;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.domain.Image} entity.
 */
@ApiModel(description = "The Image entity.\n@author A true hipster")
public class ImageDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String tmdbId;

    private String locale;

    private Float voteAverage;

    private Integer voteCount;

    @NotNull
    private LocalDate lastTMDBUpdate;


    private Long posterMovieId;

    private String posterMovieTitle;

    private Long backdropMovieId;

    private String backdropMovieTitle;

    private Long personId;

    private String personName;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public LocalDate getLastTMDBUpdate() {
        return lastTMDBUpdate;
    }

    public void setLastTMDBUpdate(LocalDate lastTMDBUpdate) {
        this.lastTMDBUpdate = lastTMDBUpdate;
    }

    public Long getPosterMovieId() {
        return posterMovieId;
    }

    public void setPosterMovieId(Long movieId) {
        this.posterMovieId = movieId;
    }

    public String getPosterMovieTitle() {
        return posterMovieTitle;
    }

    public void setPosterMovieTitle(String movieTitle) {
        this.posterMovieTitle = movieTitle;
    }

    public Long getBackdropMovieId() {
        return backdropMovieId;
    }

    public void setBackdropMovieId(Long movieId) {
        this.backdropMovieId = movieId;
    }

    public String getBackdropMovieTitle() {
        return backdropMovieTitle;
    }

    public void setBackdropMovieTitle(String movieTitle) {
        this.backdropMovieTitle = movieTitle;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageDTO)) {
            return false;
        }

        return id != null && id.equals(((ImageDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDTO{" +
            "id=" + getId() +
            ", tmdbId='" + getTmdbId() + "'" +
            ", locale='" + getLocale() + "'" +
            ", voteAverage=" + getVoteAverage() +
            ", voteCount=" + getVoteCount() +
            ", lastTMDBUpdate='" + getLastTMDBUpdate() + "'" +
            ", posterMovieId=" + getPosterMovieId() +
            ", posterMovieTitle='" + getPosterMovieTitle() + "'" +
            ", backdropMovieId=" + getBackdropMovieId() +
            ", backdropMovieTitle='" + getBackdropMovieTitle() + "'" +
            ", personId=" + getPersonId() +
            ", personName='" + getPersonName() + "'" +
            "}";
    }
}
