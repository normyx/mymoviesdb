package com.mgoulene.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.mgoulene.domain.ImageData} entity.
 */
public class ImageDataDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 40)
    private String imageSize;

    
    @Lob
    private byte[] imageBytes;

    private String imageBytesContentType;

    private Long imageId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getImageBytesContentType() {
        return imageBytesContentType;
    }

    public void setImageBytesContentType(String imageBytesContentType) {
        this.imageBytesContentType = imageBytesContentType;
    }

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ImageDataDTO)) {
            return false;
        }

        return id != null && id.equals(((ImageDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ImageDataDTO{" +
            "id=" + getId() +
            ", imageSize='" + getImageSize() + "'" +
            ", imageBytes='" + getImageBytes() + "'" +
            ", imageId=" + getImageId() +
            "}";
    }
}
