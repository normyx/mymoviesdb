package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.CreditDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Credit} and its DTO {@link CreditDTO}.
 */
@Mapper(componentModel = "spring", uses = {PersonMapper.class, MovieMapper.class})
public interface CreditMapper extends EntityMapper<CreditDTO, Credit> {

    @Mapping(source = "person.id", target = "personId")
    @Mapping(source = "person.name", target = "personName")
    @Mapping(source = "movie.id", target = "movieId")
    @Mapping(source = "movie.title", target = "movieTitle")
    CreditDTO toDto(Credit credit);

    @Mapping(source = "personId", target = "person")
    @Mapping(source = "movieId", target = "movie")
    Credit toEntity(CreditDTO creditDTO);

    default Credit fromId(Long id) {
        if (id == null) {
            return null;
        }
        Credit credit = new Credit();
        credit.setId(id);
        return credit;
    }
}
