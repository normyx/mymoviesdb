package com.mgoulene.service.mapper;


import com.mgoulene.domain.*;
import com.mgoulene.service.dto.GenreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Genre} and its DTO {@link GenreDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GenreMapper extends EntityMapper<GenreDTO, Genre> {


    @Mapping(target = "movies", ignore = true)
    @Mapping(target = "removeMovie", ignore = true)
    Genre toEntity(GenreDTO genreDTO);

    default Genre fromId(Long id) {
        if (id == null) {
            return null;
        }
        Genre genre = new Genre();
        genre.setId(id);
        return genre;
    }
}
