/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgoulene.web.rest.vm;
