import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MymoviesdbSharedModule } from 'app/shared/shared.module';
import { MymoviesdbCoreModule } from 'app/core/core.module';
import { MymoviesdbAppRoutingModule } from './app-routing.module';
import { MymoviesdbHomeModule } from './home/home.module';
import { MymoviesdbEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { MymoviesdbModule } from './mymoviesdb/mymoviesdb.module';
import { AvatarModule } from 'ngx-avatar';
import { HttpClientModule } from '@angular/common/http';
import { NgxGaugeModule } from 'ngx-gauge';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    BrowserModule,
    MymoviesdbSharedModule,
    MymoviesdbCoreModule,
    MymoviesdbHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MymoviesdbModule,
    MymoviesdbEntityModule,
    MymoviesdbAppRoutingModule,
    AvatarModule,
    NgxGaugeModule,
    HttpClientModule,
    CommonModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MymoviesdbAppModule {}
