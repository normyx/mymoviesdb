import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICredit, Credit } from 'app/shared/model/credit.model';
import { CreditService } from './credit.service';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person/person.service';
import { IMovie } from 'app/shared/model/movie.model';
import { MovieService } from 'app/entities/movie/movie.service';

type SelectableEntity = IPerson | IMovie;

@Component({
  selector: 'jhi-credit-update',
  templateUrl: './credit-update.component.html',
})
export class CreditUpdateComponent implements OnInit {
  isSaving = false;
  people: IPerson[] = [];
  movies: IMovie[] = [];
  lastTMDBUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    character: [null, [Validators.maxLength(200)]],
    creditType: [],
    department: [],
    job: [],
    order: [],
    lastTMDBUpdate: [null, [Validators.required]],
    personId: [],
    movieId: [],
  });

  constructor(
    protected creditService: CreditService,
    protected personService: PersonService,
    protected movieService: MovieService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ credit }) => {
      this.updateForm(credit);

      this.personService.query().subscribe((res: HttpResponse<IPerson[]>) => (this.people = res.body || []));

      this.movieService.query().subscribe((res: HttpResponse<IMovie[]>) => (this.movies = res.body || []));
    });
  }

  updateForm(credit: ICredit): void {
    this.editForm.patchValue({
      id: credit.id,
      tmdbId: credit.tmdbId,
      character: credit.character,
      creditType: credit.creditType,
      department: credit.department,
      job: credit.job,
      order: credit.order,
      lastTMDBUpdate: credit.lastTMDBUpdate,
      personId: credit.personId,
      movieId: credit.movieId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const credit = this.createFromForm();
    if (credit.id !== undefined) {
      this.subscribeToSaveResponse(this.creditService.update(credit));
    } else {
      this.subscribeToSaveResponse(this.creditService.create(credit));
    }
  }

  private createFromForm(): ICredit {
    return {
      ...new Credit(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      character: this.editForm.get(['character'])!.value,
      creditType: this.editForm.get(['creditType'])!.value,
      department: this.editForm.get(['department'])!.value,
      job: this.editForm.get(['job'])!.value,
      order: this.editForm.get(['order'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      personId: this.editForm.get(['personId'])!.value,
      movieId: this.editForm.get(['movieId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICredit>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
