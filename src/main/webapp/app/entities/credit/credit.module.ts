import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesdbSharedModule } from 'app/shared/shared.module';
import { CreditComponent } from './credit.component';
import { CreditDetailComponent } from './credit-detail.component';
import { CreditUpdateComponent } from './credit-update.component';
import { CreditDeleteDialogComponent } from './credit-delete-dialog.component';
import { creditRoute } from './credit.route';

@NgModule({
  imports: [MymoviesdbSharedModule, RouterModule.forChild(creditRoute)],
  declarations: [CreditComponent, CreditDetailComponent, CreditUpdateComponent, CreditDeleteDialogComponent],
  entryComponents: [CreditDeleteDialogComponent],
})
export class MymoviesdbCreditModule {}
