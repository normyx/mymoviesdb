import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'credit',
        loadChildren: () => import('./credit/credit.module').then(m => m.MymoviesdbCreditModule),
      },
      {
        path: 'genre',
        loadChildren: () => import('./genre/genre.module').then(m => m.MymoviesdbGenreModule),
      },
      {
        path: 'movie',
        loadChildren: () => import('./movie/movie.module').then(m => m.MymoviesdbMovieModule),
      },
      {
        path: 'my-movie',
        loadChildren: () => import('./my-movie/my-movie.module').then(m => m.MymoviesdbMyMovieModule),
      },
      {
        path: 'person',
        loadChildren: () => import('./person/person.module').then(m => m.MymoviesdbPersonModule),
      },
      {
        path: 'image',
        loadChildren: () => import('./image/image.module').then(m => m.MymoviesdbImageModule),
      },
      {
        path: 'image-data',
        loadChildren: () => import('./image-data/image-data.module').then(m => m.MymoviesdbImageDataModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MymoviesdbEntityModule {}
