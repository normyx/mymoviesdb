import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IImage, Image } from 'app/shared/model/image.model';
import { ImageService } from './image.service';
import { IMovie } from 'app/shared/model/movie.model';
import { MovieService } from 'app/entities/movie/movie.service';
import { IPerson } from 'app/shared/model/person.model';
import { PersonService } from 'app/entities/person/person.service';

type SelectableEntity = IMovie | IPerson;

@Component({
  selector: 'jhi-image-update',
  templateUrl: './image-update.component.html',
})
export class ImageUpdateComponent implements OnInit {
  isSaving = false;
  movies: IMovie[] = [];
  people: IPerson[] = [];
  lastTMDBUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    locale: [],
    voteAverage: [],
    voteCount: [],
    lastTMDBUpdate: [null, [Validators.required]],
    posterMovieId: [],
    backdropMovieId: [],
    personId: [],
  });

  constructor(
    protected imageService: ImageService,
    protected movieService: MovieService,
    protected personService: PersonService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ image }) => {
      this.updateForm(image);

      this.movieService.query().subscribe((res: HttpResponse<IMovie[]>) => (this.movies = res.body || []));

      this.personService.query().subscribe((res: HttpResponse<IPerson[]>) => (this.people = res.body || []));
    });
  }

  updateForm(image: IImage): void {
    this.editForm.patchValue({
      id: image.id,
      tmdbId: image.tmdbId,
      locale: image.locale,
      voteAverage: image.voteAverage,
      voteCount: image.voteCount,
      lastTMDBUpdate: image.lastTMDBUpdate,
      posterMovieId: image.posterMovieId,
      backdropMovieId: image.backdropMovieId,
      personId: image.personId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const image = this.createFromForm();
    if (image.id !== undefined) {
      this.subscribeToSaveResponse(this.imageService.update(image));
    } else {
      this.subscribeToSaveResponse(this.imageService.create(image));
    }
  }

  private createFromForm(): IImage {
    return {
      ...new Image(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      locale: this.editForm.get(['locale'])!.value,
      voteAverage: this.editForm.get(['voteAverage'])!.value,
      voteCount: this.editForm.get(['voteCount'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      posterMovieId: this.editForm.get(['posterMovieId'])!.value,
      backdropMovieId: this.editForm.get(['backdropMovieId'])!.value,
      personId: this.editForm.get(['personId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImage>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
