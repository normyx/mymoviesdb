import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMyMovie } from 'app/shared/model/my-movie.model';
import { MyMovieService } from './my-movie.service';

@Component({
  templateUrl: './my-movie-delete-dialog.component.html',
})
export class MyMovieDeleteDialogComponent {
  myMovie?: IMyMovie;

  constructor(protected myMovieService: MyMovieService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.myMovieService.delete(id).subscribe(() => {
      this.eventManager.broadcast('myMovieListModification');
      this.activeModal.close();
    });
  }
}
