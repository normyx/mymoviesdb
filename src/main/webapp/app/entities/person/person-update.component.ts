import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IPerson, Person } from 'app/shared/model/person.model';
import { PersonService } from './person.service';

@Component({
  selector: 'jhi-person-update',
  templateUrl: './person-update.component.html',
})
export class PersonUpdateComponent implements OnInit {
  isSaving = false;
  birthdayDp: any;
  deathdayDp: any;
  lastTMDBUpdateDp: any;

  editForm = this.fb.group({
    id: [],
    birthday: [],
    deathday: [],
    name: [null, [Validators.maxLength(200)]],
    aka: [null, [Validators.maxLength(200)]],
    gender: [],
    biography: [null, [Validators.maxLength(4000)]],
    placeOfBirth: [null, [Validators.maxLength(200)]],
    homepage: [null, [Validators.maxLength(200)]],
    lastTMDBUpdate: [null, [Validators.required]],
    tmdbId: [null, [Validators.required]],
  });

  constructor(protected personService: PersonService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ person }) => {
      this.updateForm(person);
    });
  }

  updateForm(person: IPerson): void {
    this.editForm.patchValue({
      id: person.id,
      birthday: person.birthday,
      deathday: person.deathday,
      name: person.name,
      aka: person.aka,
      gender: person.gender,
      biography: person.biography,
      placeOfBirth: person.placeOfBirth,
      homepage: person.homepage,
      lastTMDBUpdate: person.lastTMDBUpdate,
      tmdbId: person.tmdbId,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const person = this.createFromForm();
    if (person.id !== undefined) {
      this.subscribeToSaveResponse(this.personService.update(person));
    } else {
      this.subscribeToSaveResponse(this.personService.create(person));
    }
  }

  private createFromForm(): IPerson {
    return {
      ...new Person(),
      id: this.editForm.get(['id'])!.value,
      birthday: this.editForm.get(['birthday'])!.value,
      deathday: this.editForm.get(['deathday'])!.value,
      name: this.editForm.get(['name'])!.value,
      aka: this.editForm.get(['aka'])!.value,
      gender: this.editForm.get(['gender'])!.value,
      biography: this.editForm.get(['biography'])!.value,
      placeOfBirth: this.editForm.get(['placeOfBirth'])!.value,
      homepage: this.editForm.get(['homepage'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPerson>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
