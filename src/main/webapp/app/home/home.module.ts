import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesdbSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  imports: [MymoviesdbSharedModule, MDBBootstrapModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class MymoviesdbHomeModule {}
