import { HttpResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { PersonService } from 'app/entities/person/person.service';
import { MMDBMovieService } from 'app/mymoviesdb/mmdb-movie/mmdb-movie.service';
import { ITMDBImage } from 'app/mymoviesdb/model/tmdb-image.model';
import { ICredit } from 'app/shared/model/credit.model';
import { IPerson } from 'app/shared/model/person.model';

@Component({
  selector: 'jhi-mmdb-movie-credit',
  templateUrl: './mmdb-movie-credit.component.html',
  styleUrls: ['../../mmdb-movie/senscritique.scss'],
})
export class MMDBMovieCreditComponent implements OnInit, OnChanges {
  @Input() credit: ICredit | null = null;
  person: IPerson | null = null;
  image: ITMDBImage | null = null;
  imageUrl: string | null = null;
  avatarSize = 130;

  constructor(protected personService: PersonService, protected movieService: MMDBMovieService) {}

  loadAll(): void {
    if (this.credit) {
      this.personService.find(this.credit.personId!).subscribe((res: HttpResponse<IPerson>) => {
        this.person = res.body;
        if (this.person) {
          this.movieService.findPersonImage(this.person.id).subscribe((res2: HttpResponse<ITMDBImage>) => {
            this.image = res2.body;
            this.imageUrl = '/api/public/tmdb-image-from-tmdb/w138_and_h175_face/' + this.image?.tmdbId;
          });
        }
      });
    }
  }

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.loadAll();
  }
}
