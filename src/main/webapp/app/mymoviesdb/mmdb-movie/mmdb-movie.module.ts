import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MymoviesdbSharedModule } from 'app/shared/shared.module';
import { MMDBDurationPipe } from '../mmdb-pipe/mmdb-duration-pipe';
import { MMDBMovieDetailComponent } from './mmdb-movie-detail.component';
import { MMDBMovieComponent } from './mmdb-movie.component';
import { mmdbMovieRoute } from './mmdb-movie.route';
import { MMDBMovieCreditComponentModule } from 'app/mymoviesdb/mmdb-component/mmdb-movie-credit-component/mmdb-movie-credit.module';
import { NgxGaugeModule } from 'ngx-gauge';
import { MMDBMovieDetail2Component } from './mmdb-movie-detail2.component';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [MymoviesdbSharedModule, RouterModule.forChild(mmdbMovieRoute), MMDBMovieCreditComponentModule, NgxGaugeModule, CommonModule],
  declarations: [MMDBMovieComponent, MMDBMovieDetailComponent, MMDBMovieDetail2Component, MMDBDurationPipe],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MymoviesdbMMDBMovieModule {}
