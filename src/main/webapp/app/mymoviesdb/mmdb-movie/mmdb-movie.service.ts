import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MovieService } from 'app/entities/movie/movie.service';
import { ITMDBImage } from 'app/mymoviesdb/model/tmdb-image.model';
import { ICredit } from 'app/shared/model/credit.model';
import { IMovie } from 'app/shared/model/movie.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

type EntityResponseType = HttpResponse<IMovie>;
type EntityArrayResponseType = HttpResponse<IMovie[]>;

@Injectable({ providedIn: 'root' })
export class MMDBMovieService extends MovieService {
  constructor(protected http: HttpClient) {
    super(http);
  }

  findMoviePoster(movieId: any): Observable<HttpResponse<ITMDBImage>> {
    return this.http
      .get<ITMDBImage>(`api/movie-poster/${movieId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }
  findMovieBackdrop(movieId: any): Observable<HttpResponse<ITMDBImage>> {
    return this.http
      .get<ITMDBImage>(`api/movie-backdrop/${movieId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findPersonImage(personId: any): Observable<HttpResponse<ITMDBImage>> {
    return this.http
      .get<ITMDBImage>(`api/person-image/${personId}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findCreditCastFromMovie(movieId: any): Observable<HttpResponse<ICredit[]>> {
    return this.http
      .get<ICredit[]>(`api/mmdb-credit-cast-from-movie/${movieId}`, { observe: 'response' })
      .pipe(map((res: HttpResponse<ICredit[]>) => res));
  }

  findCreditCrewFromMovie(movieId: any): Observable<HttpResponse<ICredit[]>> {
    return this.http
      .get<ICredit[]>(`api/mmdb-credit-crew-from-movie/${movieId}`, { observe: 'response' })
      .pipe(map((res: HttpResponse<ICredit[]>) => res));
  }
}
