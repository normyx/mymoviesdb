import { Moment } from 'moment';

export interface ITMDBImage {
  tmdbId?: string;
}

export class TMDBImage implements ITMDBImage {
  constructor(public tmdbId?: string) {}
}
