import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'tmdb-movie',
        loadChildren: () => import('./tmdb-movie/tmdb-movie.module').then(m => m.MymoviesTMDBMovieModule),
      },
      {
        path: 'mmdb-movie',
        loadChildren: () => import('./mmdb-movie/mmdb-movie.module').then(m => m.MymoviesdbMMDBMovieModule),
      },
    ]),
  ],
  declarations: [],
})
export class MymoviesdbModule {}
