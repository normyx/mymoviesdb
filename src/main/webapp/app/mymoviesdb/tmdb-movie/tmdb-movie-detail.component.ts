import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMovie } from 'app/shared/model/movie.model';

@Component({
  selector: 'jhi-tmdb-movie-detail',
  templateUrl: './tmdb-movie-detail.component.html',
  styleUrls: ['tmdb-movie.scss'],
})
export class TMDBMovieDetailComponent implements OnInit {
  movie?: IMovie;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ movie }) => (this.movie = movie));
  }

  previousState(): void {
    window.history.back();
  }
}
