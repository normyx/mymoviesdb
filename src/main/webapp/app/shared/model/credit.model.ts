import { Moment } from 'moment';

export interface ICredit {
  id?: number;
  tmdbId?: string;
  character?: string;
  creditType?: string;
  department?: string;
  job?: string;
  order?: number;
  lastTMDBUpdate?: Moment;
  personName?: string;
  personId?: number;
  movieTitle?: string;
  movieId?: number;
}

export class Credit implements ICredit {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public character?: string,
    public creditType?: string,
    public department?: string,
    public job?: string,
    public order?: number,
    public lastTMDBUpdate?: Moment,
    public personName?: string,
    public personId?: number,
    public movieTitle?: string,
    public movieId?: number
  ) {}
}
