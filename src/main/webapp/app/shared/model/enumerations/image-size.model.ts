export const enum ImageSize {
  ORIGINAL = 'ORIGINAL',

  W1280 = 'W1280',

  W780 = 'W780',

  W500 = 'W500',

  W300 = 'W300',

  W185 = 'W185',

  H632 = 'H632',
}
