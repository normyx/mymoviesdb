import { Moment } from 'moment';
import { IMovie } from 'app/shared/model/movie.model';

export interface IGenre {
  id?: number;
  tmdbId?: string;
  name?: string;
  lastTMDBUpdate?: Moment;
  movies?: IMovie[];
}

export class Genre implements IGenre {
  constructor(public id?: number, public tmdbId?: string, public name?: string, public lastTMDBUpdate?: Moment, public movies?: IMovie[]) {}
}
