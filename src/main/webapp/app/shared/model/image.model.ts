import { Moment } from 'moment';

export interface IImage {
  id?: number;
  tmdbId?: string;
  locale?: string;
  voteAverage?: number;
  voteCount?: number;
  lastTMDBUpdate?: Moment;
  posterMovieTitle?: string;
  posterMovieId?: number;
  backdropMovieTitle?: string;
  backdropMovieId?: number;
  personName?: string;
  personId?: number;
}

export class Image implements IImage {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public locale?: string,
    public voteAverage?: number,
    public voteCount?: number,
    public lastTMDBUpdate?: Moment,
    public posterMovieTitle?: string,
    public posterMovieId?: number,
    public backdropMovieTitle?: string,
    public backdropMovieId?: number,
    public personName?: string,
    public personId?: number
  ) {}
}
