package com.mgoulene.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class MyMovieTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MyMovie.class);
        MyMovie myMovie1 = new MyMovie();
        myMovie1.setId(1L);
        MyMovie myMovie2 = new MyMovie();
        myMovie2.setId(myMovie1.getId());
        assertThat(myMovie1).isEqualTo(myMovie2);
        myMovie2.setId(2L);
        assertThat(myMovie1).isNotEqualTo(myMovie2);
        myMovie1.setId(null);
        assertThat(myMovie1).isNotEqualTo(myMovie2);
    }
}
