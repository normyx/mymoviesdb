package com.mgoulene.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.web.rest.TestUtil;

public class ImageDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ImageDataDTO.class);
        ImageDataDTO imageDataDTO1 = new ImageDataDTO();
        imageDataDTO1.setId(1L);
        ImageDataDTO imageDataDTO2 = new ImageDataDTO();
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
        imageDataDTO2.setId(imageDataDTO1.getId());
        assertThat(imageDataDTO1).isEqualTo(imageDataDTO2);
        imageDataDTO2.setId(2L);
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
        imageDataDTO1.setId(null);
        assertThat(imageDataDTO1).isNotEqualTo(imageDataDTO2);
    }
}
