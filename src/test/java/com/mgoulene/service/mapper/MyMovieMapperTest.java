package com.mgoulene.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MyMovieMapperTest {

    private MyMovieMapper myMovieMapper;

    @BeforeEach
    public void setUp() {
        myMovieMapper = new MyMovieMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(myMovieMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(myMovieMapper.fromId(null)).isNull();
    }
}
