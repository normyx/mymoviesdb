import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { MovieService } from 'app/entities/movie/movie.service';
import { IMovie, Movie } from 'app/shared/model/movie.model';
import { MovieStatus } from 'app/shared/model/enumerations/movie-status.model';

describe('Service Tests', () => {
  describe('Movie Service', () => {
    let injector: TestBed;
    let service: MovieService;
    let httpMock: HttpTestingController;
    let elemDefault: IMovie;
    let expectedResult: IMovie | IMovie[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(MovieService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Movie(
        0,
        'AAAAAAA',
        false,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        MovieStatus.RUMORED,
        0,
        0,
        currentDate,
        currentDate,
        'AAAAAAA',
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            releaseDate: currentDate.format(DATE_FORMAT),
            lastTMDBUpdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Movie', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            releaseDate: currentDate.format(DATE_FORMAT),
            lastTMDBUpdate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
            lastTMDBUpdate: currentDate,
          },
          returnedFromService
        );

        service.create(new Movie()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Movie', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            forAdult: true,
            homepage: 'BBBBBB',
            originalLanguage: 'BBBBBB',
            originalTitle: 'BBBBBB',
            overview: 'BBBBBB',
            tagline: 'BBBBBB',
            status: 'BBBBBB',
            voteAverage: 1,
            voteCount: 1,
            releaseDate: currentDate.format(DATE_FORMAT),
            lastTMDBUpdate: currentDate.format(DATE_FORMAT),
            tmdbId: 'BBBBBB',
            runtime: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
            lastTMDBUpdate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Movie', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            forAdult: true,
            homepage: 'BBBBBB',
            originalLanguage: 'BBBBBB',
            originalTitle: 'BBBBBB',
            overview: 'BBBBBB',
            tagline: 'BBBBBB',
            status: 'BBBBBB',
            voteAverage: 1,
            voteCount: 1,
            releaseDate: currentDate.format(DATE_FORMAT),
            lastTMDBUpdate: currentDate.format(DATE_FORMAT),
            tmdbId: 'BBBBBB',
            runtime: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            releaseDate: currentDate,
            lastTMDBUpdate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Movie', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
